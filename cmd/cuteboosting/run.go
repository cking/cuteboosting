package main

import (
	"github.com/urfave/cli"
	"gitlab.com/cking/cuteboosting/internal/app"
)

func runCommand(c *cli.Context) error {
	a := app.New(container)
	return a.Run()
}
