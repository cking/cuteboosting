package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/cking/cuteboosting/config"
	"gitlab.com/cking/cuteboosting/service"
	"gitlab.com/cking/cuteboosting/service/digen"
)

var container service.Component

func main() {
	app := cli.NewApp()

	if config.GitState == "dirty" {
		app.Version = config.LongVersion
	} else {
		app.Version = config.Version
	}

	app.Action = runCommand

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config",
			Usage: "Set the path of the configuration file",
			Value: "",
		},
	}

	app.Before = func(c *cli.Context) error {
		configFile := c.String("config")
		cfg := new(config.Config)
		cfg.Load(configFile)

		container = digen.NewDihedralComponent(service.WithConfig(cfg))

		return nil
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
