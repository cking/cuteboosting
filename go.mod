module gitlab.com/cking/cuteboosting

go 1.13

require (
	github.com/akyoto/color v1.8.11
	github.com/dimes/dihedral v0.0.0-20190924023751-620e96e21db4
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/jinzhu/configor v1.1.1
	github.com/magefile/mage v1.9.0
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/mattn/go-mastodon v0.0.5-0.20190930081922-e43f2060a867
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/urfave/cli v1.22.2
	gitlab.com/bakyun/magician v0.0.0-20190210101925-cf313cf20e1e
	go.uber.org/zap v1.13.0
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
