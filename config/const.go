package config

// Application specific constants
const (
	ApplicationName = "cuteboosting"
)

// provided by buildscript
var (
	GitCommit   = "unknown"
	GitBranch   = "none"
	GitState    = "dirty"
	GitSummary  = "dirty"
	BuildDate   = "now"
	Version     = "0.0.0"
	LongVersion = "0.0.0-dirty"
)
