package config

import (
	"os"

	"github.com/jinzhu/configor"
	"github.com/mattn/go-mastodon"
	"gopkg.in/yaml.v2"
)

// Config parses the config file
type Config struct {
	Mastodon struct {
		Instance     string      `required:"true" yaml:"instance"`
		Username     string      `required:"true" yaml:"username"`
		Password     string      `required:"true" yaml:"password"`
		ClientID     string      `yaml:"client_id"`
		ClientSecret string      `yaml:"client_secret"`
		LastBoost    mastodon.ID `yaml:"last_boost"`
	} `yaml:"mastodon"`

	Logging []Logging `yaml:"logging"`

	file string
}

// Load the configuration
func (c *Config) Load(file string) error {
	if file == "" {
		file = ApplicationName + ".yaml"
	}

	err := configor.New(&configor.Config{
		Debug:                false,
		Verbose:              false,
		Silent:               true,
		ErrorOnUnmatchedKeys: false,
		ENVPrefix:            ApplicationName,
	}).Load(
		c,
		file,
	)

	if err != nil {
		return err
	}

	c.file = file
	return nil
}

// Save the current config to disk
func (c *Config) Save() error {
	f, err := os.OpenFile(c.file, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, os.ModePerm)
	if err != nil {
		return err
	}

	return yaml.NewEncoder(f).Encode(c)
}
