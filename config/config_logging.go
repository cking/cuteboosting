package config

import (
	"go.uber.org/zap/zapcore"
)

// LoggingType for output
type LoggingType string

// The different outputs
const (
	LoggingTypeFile    = "file"
	LoggingTypeConsole = "console"

	loggingTypeUnset = ""
)

// LoggingFormat selection
type LoggingFormat string

// The different formats
const (
	LoggingFormatText LoggingFormat = "text"
	LoggingFormatJSON               = "json"
)

// Logging settings
type Logging struct {
	Type        LoggingType   `yaml:"type"`
	File        string        `yaml:"file"`
	Format      LoggingFormat `yaml:"format"`
	MinLogLevel zapcore.Level `yaml:"min_log_level"`
	MaxLogLevel zapcore.Level `yaml:"max_log_level"`
}
