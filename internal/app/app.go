package app

import (
	"context"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/microcosm-cc/bluemonday"

	"github.com/mattn/go-mastodon"
	"gitlab.com/cking/cuteboosting/config"
	"gitlab.com/cking/cuteboosting/service"
	"go.uber.org/zap"
)

func New(di service.Component) *App {
	return &App{
		di: di,
	}
}

// App interface
type App struct {
	di service.Component
}

// Run application
func (a *App) Run() error {
	logger := a.di.GetLogger()
	compiled, _ := time.Parse(time.RFC3339, config.BuildDate)
	logger.Info(
		config.ApplicationName+" starting up",
		zap.Time("compiled", compiled),
		zap.String("version", config.LongVersion),
	)
	c := a.di.GetConfiguration()
	m := a.di.GetMastodon()

	stopChan := make(chan os.Signal, 1)
	signal.Notify(stopChan, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	logger.Info("bot started, use ^c to stop")

	statusChan := make(chan *mastodon.Status)
	errChan := make(chan error)

	defer c.Save()
	go func(m *mastodon.Client) {
		p := new(mastodon.Pagination)
		if c.Mastodon.LastBoost != "" {
			p.SinceID = c.Mastodon.LastBoost
		}

		for {
			statuses, err := m.GetTimelineHashtag(context.Background(), "cuteposting", false, p)
			if err != nil {
				errChan <- err
				return
			}

			logger.Debug("found new statuses", zap.Int("count", len(statuses)))
			if len(statuses) > 0 {
				p.SinceID = statuses[0].ID

				for index := len(statuses) - 1; index >= 0; index-- {
					statusChan <- statuses[index]
				}

				c.Mastodon.LastBoost = p.SinceID
			}

			time.Sleep(time.Minute)
		}
	}(m)

	strict := bluemonday.StrictPolicy()
	for {
		select {
		case status := <-statusChan:
			rawContent := strict.Sanitize(status.Content)

			logger.Debug("processing status", zap.String("content", rawContent))

			if !strings.Contains(rawContent, "#cuteposting") {
				continue
			}
			m.Reblog(context.Background(), status.ID)

		case err := <-errChan:
			logger.Error("received error", zap.Error(err))
			return err

		case sig := <-stopChan:
			logger.Debug(
				"received stop signal",
				zap.Stringer("signal", sig),
			)
			return nil
		}
	}
}
