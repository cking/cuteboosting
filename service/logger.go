package service

import (
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/akyoto/color"
	"gitlab.com/cking/cuteboosting/config"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func logLevelBetween(min, max zapcore.Level) zap.LevelEnablerFunc {
	return zap.LevelEnablerFunc(func(l zapcore.Level) bool {
		return l >= min && l <= max
	})
}

// LoggerModule definition
type LoggerModule struct{}

// ProvidesLogger creates the logger instance with all writers attached
func (m *LoggerModule) ProvidesLogger(cfg *config.Config) *zap.Logger {
	logging := cfg.Logging
	if len(logging) == 0 {
		logging = []config.Logging{
			{},
		}
	}

	var cores []zapcore.Core
	for _, c := range logging {
		enccfg := zapcore.EncoderConfig{
			MessageKey:     "msg",
			LevelKey:       "level",
			TimeKey:        "time",
			NameKey:        "name",
			CallerKey:      "caller",
			StacktraceKey:  "stack",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.CapitalColorLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
			EncodeDuration: zapcore.NanosDurationEncoder,
			EncodeName:     zapcore.FullNameEncoder,
		}

		enc := zapcore.NewJSONEncoder(enccfg)
		if c.Format != config.LoggingFormatJSON {
			enc = zapcore.NewConsoleEncoder(enccfg)
		}

		var out io.Writer = color.Output
		switch c.Type {
		case config.LoggingTypeConsole:
			if c.File == "stderr" {
				out = color.Error
			}
		case config.LoggingTypeFile:
			var err error
			fp, _ := filepath.Abs(c.File)
			base := filepath.Dir(fp)
			if _, err := os.Stat(base); os.IsNotExist(err) {
				if err := os.MkdirAll(base, os.ModeDir|os.ModePerm); err != nil {
					panic(fmt.Errorf("failed to create logging direcctory: %w", err))
				}
			}

			out, err = os.OpenFile(fp, os.O_CREATE|os.O_APPEND, os.ModePerm)
			if err != nil {
				panic(fmt.Errorf("failed to open logging file: %w", err))
			}
		}

		if c.MinLogLevel <= 0 {
			c.MinLogLevel = zapcore.InfoLevel
			if config.Debug {
				c.MinLogLevel = zapcore.DebugLevel
			}
		}

		if c.MaxLogLevel == 0 || c.MaxLogLevel > zapcore.FatalLevel {
			c.MaxLogLevel = zapcore.FatalLevel
		}

		cores = append(cores, zapcore.NewCore(enc, zapcore.AddSync(out), logLevelBetween(c.MinLogLevel, c.MaxLogLevel)))
	}

	tee := zapcore.NewTee(cores...)
	var options []zap.Option
	if config.Debug {
		options = []zap.Option{
			zap.AddCaller(),
			zap.AddStacktrace(logLevelBetween(zapcore.WarnLevel, 0)),
			zap.Development(),
		}
	}
	return zap.New(tee, options...)
}
