package service

import (
	"github.com/dimes/dihedral/embeds"
	"gitlab.com/cking/cuteboosting/config"
)

// ConfigModule for the service container
type ConfigModule struct {
	provided embeds.ProvidedModule
	Config   *config.Config
}

// ProvidesConfig module
func (m *ConfigModule) ProvidesConfig() *config.Config {
	return m.Config
}

// WithConfig returns the config module with the specified config
func WithConfig(cfg *config.Config) *ConfigModule {
	return &ConfigModule{Config: cfg}
}
