package service

import (
	"github.com/mattn/go-mastodon"
	"gitlab.com/cking/cuteboosting/config"
	"go.uber.org/zap"
)

// Component of the DI container
type Component interface {
	GetConfiguration() *config.Config
	GetLogger() *zap.Logger
	GetMastodon() *mastodon.Client
}
