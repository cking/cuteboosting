//go:generate go run github.com/dimes/dihedral -definition Definition

package service

// Definition of the service container
type Definition interface {
	Modules() (*ConfigModule, *LoggerModule, *MastodonModule)

	Target() Component
}
