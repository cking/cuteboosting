package service

import (
	"context"
	"fmt"

	"github.com/mattn/go-mastodon"
	"gitlab.com/cking/cuteboosting/config"
)

// MastodonModule definition
type MastodonModule struct {
	client *mastodon.Client
}

// ProvidesMastodon creates the mastodon client
func (m *MastodonModule) ProvidesMastodon(cfg *config.Config) (*mastodon.Client, error) {
	if m.client != nil {
		return m.client, nil
	}

	if cfg.Mastodon.ClientID == "" || cfg.Mastodon.ClientSecret == "" {
		app, err := mastodon.RegisterApp(context.Background(), &mastodon.AppConfig{
			Server:     cfg.Mastodon.Instance,
			ClientName: config.ApplicationName,
			Scopes:     "read write follow",
		})

		if err != nil {
			return nil, fmt.Errorf("Failed to create OAuth application: %w", err)
		}

		cfg.Mastodon.ClientID = app.ClientID
		cfg.Mastodon.ClientSecret = app.ClientSecret
		if err := cfg.Save(); err != nil {
			return nil, fmt.Errorf("failed to update config with oauth details: %w", err)
		}
	}

	m.client = mastodon.NewClient(&mastodon.Config{
		Server:       cfg.Mastodon.Instance,
		ClientID:     cfg.Mastodon.ClientID,
		ClientSecret: cfg.Mastodon.ClientSecret,
	})

	if err := m.client.Authenticate(context.Background(), cfg.Mastodon.Username, cfg.Mastodon.Password); err != nil {
		return nil, fmt.Errorf("failed to authenticate mastodon user: %w", err)
	}

	return m.client, nil
}
